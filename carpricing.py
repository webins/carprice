# -*- coding: utf-8 -*-
"""
广义线性模型定价模块，整合了pandas、numpy、statsmodels的相关功能。
数据框(DataFrame)要求必须包括机构维度及至少一个定价维度，及以下数据列：
满期保费、满期车年、赔案件数、赔款金额。
数据列名中不能使用：
freq、avg、freq_pred、avg_pred、loss_pred、loss_ratio_pred、score(计算过程中会自动生成这些列)。
前述列名分别表示：出险频度、案均赔款、模型预测出险频度、模型预测案均赔款、模型预测赔款、模型预测赔付率、风险评分。
定价可分步计算，也可以一次性处理完毕：
    若分步计算，则须依次使用以下函数（注意参数要求）：
    导入模块：import carpricing
    创建对象：price=carpricing.CarPrice()
    设置数据：price.setData(数据框)
    案均模型：price.mod_freq(),结果放置在对象的mod_freq_res属性中，预测数据放置在modData1中。
    频度模型：price.mod_avg()，结果放置在对象的mod_avg_res属性中，预测数据放置在modData2中。
    预测结果：price.predict()，data增加frq_pred、avg_pred、loss_pred三个列。
    评分计算:price.score()，结果放置到pred_data属性中。
    生成原始费率表：price.calRateTable()
    生成最终费率表和基准纯风险保费：price.calRateTableAdj()
    评分线图（可选）：price.plotScore()
    保存预测结果（可选）：price.pred_data.to_csv()
    保存结果（可选）：price.save()
如果想根据原始数据（data属性)一次性生成费率表，请使用对象.rate()方法，并指定过程中所需要的参数。
Created on Wed Dec 19 10:33:30 2018
@author: baikuiyao01
"""
import numpy as np
import pandas as pd
import statsmodels.api as sm
import statsmodels.formula.api as smf
from statsmodels import graphics
import matplotlib.pyplot as plt
from scipy import stats
import os

class CarPrice:
    '''
    定价所使用的类。由于需要指定关键数据列，
    所以基础数据必须对象的setData()方法，不能用对象.data属性直接赋值。
    '''
    
    def __init__(self):
        '''
        构造函数。
        自动引入statsmodels.api和statsmodels.formula.api。
        '''
        self.smf=smf
        self.sm=sm
        self.data=None
        self.modData=None
        self.groupNum=100
    def setData(self,data,earnedPremium='ewt',earnedPolicyCnt='ee',lossCnt='paidCnt',lossSum='paidSum',dept='dept'):
        '''
        设置基础数据.并明确指定参数列出的数据列。
        参数：
        data:pandas的dataframe数据类型
        earnedPremium: 满期保费列名，默认ewt
        earnedPolicyCnt：满期车年数列，默认ee
        lossCnt:案件数列，默认paidCnt
        lossSum：赔款列,默认paidSum
        dept:机构维度，用于分机构评基准
        结果：
        对象的data属性，pandas的dataframe数据类型
        '''
        self.data=data
        data.rename(columns={earnedPremium:'ewt',earnedPolicyCnt:'ee',lossCnt:'paidCnt',lossSum:'paidSum',dept:'dept'},inplace=True)
        
    def mod_freq(self,dim,freq_weights='ee'):
        '''
        GLM模型函数，封装的statsmodels.formula.api的glm函数。
        注意：modData将会增加一列:freq，注意列名不要重复。
        参数：
        dim:定价维度，list列表，字段名必须为data的列名
        freq_weights:加权因子，默认为满期车年，如修改请指定data中的列名。
        '''
        y_top='paidCnt'
        y_bottom='ee'
        self.modData1=self.data.groupby(dim).sum()
        self.modData1.reset_index(inplace=True)
        self.modData1['freq']=self.modData1[y_top]/self.modData1[y_bottom]
        formula='freq~'
        for i,el in enumerate(dim):
            if i==0:
                formula=formula+el
            else:
                formula=formula+'+'+el
        self.mod_freq_res=smf.glm(formula=formula,data=self.modData1,family=sm.families.Poisson(link=sm.families.links.log),freq_weights=self.modData1[freq_weights]).fit()
        self.modData1['freq_pred']=self.mod_freq_res.fittedvalues
    def mod_avg(self,dim,freq_weights='ee'):
        '''
        GLM模型函数，封装的statsmodels.formula.api的glm函数。
        注意：modData将会增加一列:avg，注意列名不要重复。
        参数：
        dim:list列表，字段名必须为data的列名
        y_top:计算y变量的分子
        y_bottom:计算y变量的分母
        freq_weights:加权因子，默认为满期件数，如修改请指定data中的列名。
        '''
        y_top='paidSum'
        y_bottom='paidCnt'
        self.modData2=self.data.groupby(dim).sum()
        self.modData2.reset_index(inplace=True)
        self.modData2['avg']=self.modData2[y_top]/self.modData2[y_bottom]
        formula='avg~'
        for i,el in enumerate(dim):
            if i==0:
                formula=formula+el
            else:
                formula=formula+'+'+el
        self.mod_avg_res=smf.glm(formula=formula,data=self.modData2,family=sm.families.Gamma(link=sm.families.links.log),freq_weights=self.modData2[freq_weights]).fit()
        self.modData2['avg_pred']=self.mod_avg_res.fittedvalues
    def predict(self):
        '''
        对清单数据data的每行预测出险频度和案均赔款、赔款，
        并放在data属性的freq_pred、avg_pred、loss_pred列。
        '''
        self.data['freq_pred']=self.mod_freq_res.predict(self.data)
        self.data['avg_pred']=self.mod_avg_res.predict(self.data)
        self.data['loss_pred']=self.data['freq_pred']*self.data['avg_pred']*self.data['ee']
    def score(self,groupNum=100):
        '''
        对保单清单按预期赔付率评分，默认为10分。
        参数:
        groupNum：评分的组数，默认为10组。
        '''
        self.groupNum=groupNum
        self.pred_data=self.data[(self.data['ewt']!=0) & (self.data['loss_pred']!=0)]
        self.pred_data['loss_ratio_pred']=self.pred_data['loss_pred']/self.pred_data['ewt']
        min_ratio=self.pred_data['loss_ratio_pred'].min()
        max_ratio=self.pred_data['loss_ratio_pred'].max()
        level=pow(max_ratio/min_ratio,1/(groupNum-2))-1
        self.pred_data['score']=self.pred_data['loss_ratio_pred'].apply(lambda x:np.median([int(np.log10(x/min_ratio)/np.log10(1+level))+1,1,groupNum]))
        
    def plotScore(self):
        '''
        按预期赔付率画出评分图。
        '''
        plotData=self.pred_data.groupby(['score']).sum()
        plotData.reset_index(inplace=True)
        plotData['pred_loss_ratio']=plotData['loss_pred']/plotData['ewt']
        plotData['actual_ratio']=plotData['paidSum']/plotData['ewt']
        self.score_data=plotData
        fig=plt.figure()
        ax=fig.add_subplot(111)
        ax.plot(plotData['score'],plotData['pred_loss_ratio'],label='predict loss ratio',color='red')
        ax.plot(plotData['score'],plotData['actual_ratio'],label='actual loss ratio',color='blue')
        ax.set_title("loss ratio score result")
        ax.set_xlabel("score")
        ax.set_ylabel("loss ratio")
        ax.set_xticks(range(self.groupNum))
        ax2=ax.twinx()
        ax2.set_ylabel("EE")
        ax2.bar(plotData['score'],plotData['ee'],alpha=0.5,color='seagreen')
        plt.show()
    def plotFreqResid(self):
        resid=self.mod_freq_res.resid_deviance.copy()
        graphics.gofplots.qqplot(resid, line='r')
    def plotAvgResid(self):
        resid=self.mod_avg_res.resid_deviance.copy()
        graphics.gofplots.qqplot(resid, line='r')
    def plotFreqHistResid(self):
        fig, ax = plt.subplots()
        resid=self.mod_freq_res.resid_deviance.copy()
        resid_std = stats.zscore(resid)
        ax.hist(resid_std, bins=25)
        ax.set_title('Histogram of freq standardized deviance residuals')
    def plotAvgHistResid(self):
        fig, ax = plt.subplots()
        resid=self.mod_avg_res.resid_deviance.copy()
        resid_std = stats.zscore(resid)
        ax.hist(resid_std, bins=25)
        ax.set_title('Histogram of avg standardized deviance residuals')
    def calRateTable(self):
        '''
        得出费率系数表（附p值），并保存在对象的rateTable中（rate列即为费率系数）。
        此费率表为模型费率表，基准和因子未平衡。
        '''
        freq_cov=self.mod_freq_res.params.to_frame(name='freq_cov')
        freq_p=self.mod_freq_res.pvalues.to_frame(name='freq_pvalue')
        table=freq_cov.merge(freq_p,how='outer',left_index=True,right_index=True)
        avg_cov=self.mod_avg_res.params.to_frame(name='avg_cov')
        avg_p=self.mod_avg_res.pvalues.to_frame(name='avg_pvalue')
        table_t=avg_cov.merge(avg_p,how='outer',left_index=True,right_index=True)
        self.rateTable=table.merge(table_t,how='outer',left_index=True,right_index=True)
        self.rateTable.reset_index(inplace=True)
        self.rateTable['facType']=self.rateTable['index'].apply(lambda x:x.split('[')[0])
        self.rateTable['fac']=self.rateTable['index'].apply(lambda x:x.replace("]",'').split("T.")[-1])
        l=list(set(self.rateTable['facType']))
        for i in l:
            if i in self.data.columns:
                fac=list(set(self.data[i].unique()).difference(set(self.rateTable[self.rateTable['facType']==i]['fac'])))[0]
                facType=i
                self.rateTable=self.rateTable.append({'freq_cov':0, 'freq_pvalue':0, 'avg_cov':0, 'avg_pvalue':0,'facType':facType,  'fac':fac},ignore_index=True)
        del self.rateTable['index']
        f=self.rateTable['freq_cov'].apply(lambda x:np.exp(x))
        g=self.rateTable['avg_cov'].apply(lambda x:np.exp(x))
        self.rateTable['rate']=f*g
        self.rateTable=self.rateTable[['facType', 'fac','freq_cov', 'freq_pvalue', 'avg_cov', 'avg_pvalue','rate']]
    def calRateTableAdj(self,province=False):
        '''
        基准和因子校准后的费率表。每个费率因子值的加权值为1,基准默认不分机构。
        校准后的费率表保存在对象的rateTableAdj属性中,基准保存在对象的base属性里。
        参数：
        province：是否按机构校准基准。默认为否。
        '''
        adj=self.rateTable.copy()
        l=list(set(adj['facType']))
        data_t=self.data.copy()
        for i in l:
            if i in list(data_t.columns):
                d=adj.loc[adj['facType']==i,['fac','rate']]
                d.rename(columns={'rate':i+'_V'},inplace=True)
                data_t=data_t.merge(d,how='left',left_on=i,right_on='fac')
        for i in l:
            if i in list(data_t.columns):
                scale=(data_t[i+'_V']*data_t['ee']).sum()/data_t['ee'].sum()
                adj.loc[adj['facType']==i,['rate']]=adj['rate']/scale
        self.rateTable_Adj=adj
        data_t['total_fac']=1
        if province==False:
            for i in l:
                if i in list(data_t.columns):
                    data_t['total_fac']*=data_t[i+'_V']
            self.base=pd.DataFrame({'dept':['ALL',],'base':[data_t['paidSum'].sum()/(data_t['total_fac']*data_t['ee']).sum(),]})
        else:
            for i in l:
                if i in list(data_t.columns):
                    data_t['total_fac']*=data_t[i+'_V']
            data_t['total_fac_ee']=data_t['total_fac']*data_t['ee']
            if 'dept' in list(data_t.columns):
                _t=data_t.groupby(['dept']).sum()
                _t.reset_index(inplace=True)
                _t['base']=_t.paidSum/_t.total_fac_ee
                self.base=_t[['dept','base']]
    def save(self,path,saveAll=False):
        '''       
        在指定目录保存以下以结果：
            pred_data.csv:带有预测值和评分结果的原始清单数据
            score_data.csv:评分等级对应的直实赔付率和预测赔付率的对比数据
            freq_model_data.csv:带有拟合值的出险频度的维度分组数据
            avg_model_data.csv:带有拟合值的案均赔款的维度分组数据
            freq_model：出险频度模型（二进制数据，不可直接读取）
            avg_model：案均赔款模型（二进制数据，不可直接读取）
            rateTable.csv：原始模型的费率表（因子和基准未校正），用于检验原始模型
            rateTable_adj.csv：校正基准和因子后的最终费率表
            self.base.csv：基准纯风险保费
        path:保存结果的目录。
        saveAll:保存所有相关数据，包括清单，默认为否，仅保存评分结果、费率表和基准。
        '''
        if saveAll==True:
            self.pred_data.to_csv(os.path.join(path,"pred_data.csv"),encoding="gb18030")
            self.mod_freq_res.save(os.path.join(path,"freq_model"),encoding="gb18030")
            self.mod_avg_res.save(os.path.join(path,"avg_model"),encoding="gb18030")
        self.score_data.to_csv(os.path.join(path,"score_data.csv"),encoding="gb18030")
        self.modData1.to_csv(os.path.join(path,"freq_model_data.csv"),encoding="gb18030")
        self.modData2.to_csv(os.path.join(path,"avg_model_data.csv"),encoding="gb18030")
        self.rateTable.to_csv(os.path.join(path,"rateTable.csv"),encoding="gb18030")
        self.rateTable_Adj.to_csv(os.path.join(path,"rateTable_adj.csv"),encoding="gb18030")
        self.base.to_csv(os.path.join(path,"base.csv"),encoding="gb18030")
    def report(self):
        '''
        显示模型信息。
        '''
        print("----------------出险频度拟合情况------------------")
        print(self.mod_freq_res.summary2())
        print("----------------案均赔款拟合情况------------------")
        print(self.mod_avg_res.summary2())
        print("----------------定价费率因子表--------------------")
        print(self.rateTable_Adj)
        print("----------------基准纯风险保费表------------------")
        print(self.base)
        print("----------------保单评分校验图--------------------")
        self.score(groupNum=100)
        self.plotScore()
    def rate(self,dim,freq_freq_weights='ee',avg_freq_weights='ee',groupNum=100):
        '''
        对基础数据建模、生成费率表和基准，并生成评分结果。
        是本对象其它函数方法的整合。
        '''
        self.mod_avg(dim,freq_weights=avg_freq_weights)
        print("[PASS]案均赔款模型建模完成！")
        self.mod_freq(dim,freq_weights=freq_freq_weights)
        print("[PASS]案均赔款模型建模完成！")
        self.predict()
        print("[PASS]清单数据预测完成！")
        self.score(groupNum=groupNum)
        print("[PASS]保单评分完成！")
        self.plotScore()
        print("[PASS]评分图完成！")
        self.calRateTable()
        print("[PASS]模型费率表完成！")
        self.calRateTableAdj(province=True)
        print("[PASS]模型费率表已校正完成！")
        self.report()
        print("[PASS]已生成结果，流程处理完毕！")