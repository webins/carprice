# carprice

#### 介绍
广义线性模型定价模块，实现的功能如下：  
1.按出险频度和案均赔款分别建立GLM回归模型（出险频度为泊松分布，案均赔款为伽马分布，连接函数均为对象联接函数）。  
2.根据模型结果自动生成费率表和分地区基准纯风险保费表。    
3.根据模型对保单进行评分，并可按10或100分评分校验模型拟合效果。  

#### 软件架构
1.python3以上版本  
2.依赖的包：statsmodels,numpy,pandas,matplotlib,scipy  

#### 安装教程
1.下载carpricing.py后，放到python的指定目录下，如C:\PYTHON。  
2.切换PYTHON工作目录到该目录：
import os  
os.chdir("C:\PYTHON")

#### 使用说明
最简单的使用方法如下：  
1.引入本模块：import carpricing  
2.创建对象：price=carpricing.CarPrice()  
3.引入pandas模块：import pandas as pd  
4.读入定价基础数据：  
data=pd.read_csv(csv文件,encoding="gb18030")  
price.setData(data)  
5.建模：price.rate()  
6.输出费率表、基准纯风险保费等：  
price.rate()//显示结果  
price.save()//保存结果  

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)